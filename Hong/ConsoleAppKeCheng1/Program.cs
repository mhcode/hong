﻿using System;

using ConsoleAppKeCheng1.车类;

namespace ConsoleAppKeCheng1
{//命名空间
    class Program
    {//类
        static void Main(string[] args)
        {//方法\函数
            //Console.WriteLine("Hello World!");

            /*
            var car = new 车("宝马");
            //car.车名 = "";
            car.轮子数量 = 4;
            Console.WriteLine(car.车名);
            Console.ReadLine();
            */

            string A = "1";
            //int  B = 1;
            //double C = 1d;
            //bool D = false;

            var B = Convert.ToInt32(A);//数据类型的转换
            var C = Convert.ToDouble(A);//数据类型的转换
            var D = Convert.ToDecimal(A);//数据类型的转换
            var E = Convert.ToSingle(A);//数据类型的转换


            //循环
            for (int i = 0; i < 10; i++)
            {
                //...循环做某事
                continue;//跳过当前循环，并继续循环
                break;//跳出循环
            }

            while (true)
            {
                break;
            }

            //运算加减乘除
            Console.WriteLine(1+1);//加法运算
            Console.WriteLine(1-1);//减法运算
            Console.WriteLine(1*2);//乘法运算
            Console.WriteLine(4/2);//除法运算
            Console.WriteLine(5%2);//求余运算

            //单行注释，程序不执行本行代码

            /*
            多行注释
            在这个标识内的都不会被执行
            */

            Console.Read();

            
        }
    }
}
