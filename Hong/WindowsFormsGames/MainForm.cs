﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsGames
{
    public partial class MainForm : Form
    {
        public 人 自己 = new 人()
        {
            姓名 = "宏智",
            剩余魔法值 = 100,
            剩余血量 = 100
        };

        public 人 敌人 = new 人()
        {
            姓名 = "小偷",
            剩余魔法值 = 100,
            剩余血量 = 100
        };

        public MainForm()
        {
            InitializeComponent();

            lbl我方.Text = "我方单位:" + 自己.姓名;
            lbl敌方.Text = "敌方单位:" + 敌人.姓名;

           

            pg我方血量.Value = 自己.剩余血量;//设置血量条为100
            pg敌方血量.Value = 敌人.剩余血量;

            pg我方魔法值.Value = 自己.剩余魔法值;
            pg敌方魔法值.Value = 敌人.剩余魔法值;

            rich日志.AppendText("双方进入了战斗" + Environment.NewLine);//Environment.NewLine为换行符//AppendText为向富文本框追加文本
            rich日志.AppendText("我方血量" + pg我方血量.Value + Environment.NewLine);
            rich日志.AppendText("敌方血量" + pg敌方血量.Value + Environment.NewLine);
        }

        private void btn技能1_Click(object sender, EventArgs e)
        {
            自己.普通攻击(敌人);
            if (敌人.剩余血量 < 0)
                敌人.剩余血量 = 0;
            if (自己.剩余魔法值 < 0)
                自己.剩余魔法值 = 0;
            rich日志.AppendText(自己.姓名 + "攻击了" + 敌人.姓名 + Environment.NewLine);
            pg敌方血量.Value = 敌人.剩余血量;
            pg我方魔法值.Value = 自己.剩余魔法值;

            反击方法();
            检查阵亡();
        }

        private void 检查阵亡()
        {
            bool 死了 = false;
            if (自己.剩余血量 == 0 && 敌人.剩余血量 == 0)
            {
                MessageBox.Show("同归于尽了");
                死了 = true;
            }
            if (自己.剩余血量 == 0)
            {
                MessageBox.Show("自己阵亡了");
                死了 = true;
            }
            if (敌人.剩余血量 == 0)
            {
                MessageBox.Show("敌人阵亡了");
                死了 = true;
            }
            if (死了)
            {
                btn技能1.Enabled = false;
                btn技能2.Enabled = false;
            }
        }

        private void 反击方法()
        {
            Random random = new Random();
            var result = random.Next(3);
            if (result == 0)
            {
                敌人.普通攻击(自己);
                if (自己.剩余血量 < 0)
                    自己.剩余血量 = 0;
                if (敌人.剩余魔法值 < 0)
                    敌人.剩余魔法值 = 0;
                rich日志.AppendText("敌人使用了普通攻击进行反击" + Environment.NewLine);
            }
            if (result == 1)
            {
                if (敌人.剩余魔法值 < 10)
                {
                    敌人.普通攻击(自己);
                    if (自己.剩余血量 < 0)
                        自己.剩余血量 = 0;
                    if (敌人.剩余魔法值 < 0)
                        敌人.剩余魔法值 = 0;
                    rich日志.AppendText("敌人因为没有了魔法值,使用了普通攻击进行反击" + Environment.NewLine);
                    //rich日志.AppendText("敌人使用了技能1进行反击,但是没有了魔法值,哑火了" + Environment.NewLine);
                    return;
                }

                敌人.技能1(自己);
                if (自己.剩余血量 < 0)
                    自己.剩余血量 = 0;
                if (敌人.剩余魔法值 < 0)
                    敌人.剩余魔法值 = 0;
                rich日志.AppendText("敌人使用了技能1进行反击" + Environment.NewLine);
            }
            if (result == 2)
            {
                敌人.技能2(自己);
                if (自己.剩余血量 < 0)
                    自己.剩余血量 = 0;
                if (敌人.剩余魔法值 < 0)
                    敌人.剩余魔法值 = 0;
                rich日志.AppendText("敌人使用了技能2进行反击" + Environment.NewLine);
            }
            if (result == 3)
            {
                敌人.技能3(自己);
                if (自己.剩余血量 < 0)
                    自己.剩余血量 = 0;
                if (敌人.剩余魔法值 < 0)
                    敌人.剩余魔法值 = 0;
                rich日志.AppendText("敌人使用了技能3进行反击" + Environment.NewLine);
            }

            pg我方魔法值.Value = 自己.剩余魔法值;
            pg敌方魔法值.Value = 敌人.剩余魔法值;
            pg我方血量.Value = 自己.剩余血量;
            pg敌方血量.Value = 敌人.剩余血量;

            rich日志.AppendText("我方血量" + pg我方血量.Value + Environment.NewLine);
            rich日志.AppendText("敌方血量" + pg敌方血量.Value + Environment.NewLine);
            rich日志.AppendText("我方魔法值" + pg我方魔法值.Value + Environment.NewLine);
            rich日志.AppendText("敌方魔法值" + pg敌方魔法值.Value + Environment.NewLine);
        }

        private void btn技能2_Click(object sender, EventArgs e)
        {
            if (自己.剩余魔法值 < 10)
            {
                rich日志.AppendText("没蓝了,无法使用");
                return;
            }

            自己.技能2(敌人);

            if (敌人.剩余血量 < 0)
                敌人.剩余血量 = 0;
            if (自己.剩余魔法值 < 0)
                自己.剩余魔法值 = 0;

            rich日志.AppendText(自己.姓名 + "使用了封魔斩攻击了" + 敌人.姓名 + Environment.NewLine);
            pg敌方血量.Value = 敌人.剩余血量;
            pg我方魔法值.Value = 自己.剩余魔法值;

            反击方法();
            检查阵亡();
        }
    }
}
