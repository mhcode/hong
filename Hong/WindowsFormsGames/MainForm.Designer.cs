﻿namespace WindowsFormsGames
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pg我方血量 = new WindowsFormsGames.血量进度条();
            this.pg敌方血量 = new WindowsFormsGames.血量进度条();
            this.lbl我方 = new System.Windows.Forms.Label();
            this.lbl敌方 = new System.Windows.Forms.Label();
            this.rich日志 = new System.Windows.Forms.RichTextBox();
            this.btn技能1 = new System.Windows.Forms.Button();
            this.btn技能2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.pg敌方魔法值 = new WindowsFormsGames.魔法进度条();
            this.pg我方魔法值 = new WindowsFormsGames.魔法进度条();
            this.SuspendLayout();
            // 
            // pg我方血量
            // 
            this.pg我方血量.Location = new System.Drawing.Point(12, 94);
            this.pg我方血量.Name = "pg我方血量";
            this.pg我方血量.Size = new System.Drawing.Size(290, 23);
            this.pg我方血量.TabIndex = 0;
            // 
            // pg敌方血量
            // 
            this.pg敌方血量.Location = new System.Drawing.Point(498, 94);
            this.pg敌方血量.Name = "pg敌方血量";
            this.pg敌方血量.Size = new System.Drawing.Size(290, 23);
            this.pg敌方血量.TabIndex = 1;
            // 
            // lbl我方
            // 
            this.lbl我方.AutoSize = true;
            this.lbl我方.Location = new System.Drawing.Point(12, 63);
            this.lbl我方.Name = "lbl我方";
            this.lbl我方.Size = new System.Drawing.Size(59, 12);
            this.lbl我方.TabIndex = 2;
            this.lbl我方.Text = "我方名称:";
            // 
            // lbl敌方
            // 
            this.lbl敌方.AutoSize = true;
            this.lbl敌方.Location = new System.Drawing.Point(501, 63);
            this.lbl敌方.Name = "lbl敌方";
            this.lbl敌方.Size = new System.Drawing.Size(59, 12);
            this.lbl敌方.TabIndex = 3;
            this.lbl敌方.Text = "敌方名称:";
            // 
            // rich日志
            // 
            this.rich日志.Location = new System.Drawing.Point(217, 232);
            this.rich日志.Name = "rich日志";
            this.rich日志.Size = new System.Drawing.Size(388, 169);
            this.rich日志.TabIndex = 4;
            this.rich日志.Text = "";
            // 
            // btn技能1
            // 
            this.btn技能1.Location = new System.Drawing.Point(174, 160);
            this.btn技能1.Name = "btn技能1";
            this.btn技能1.Size = new System.Drawing.Size(98, 43);
            this.btn技能1.TabIndex = 5;
            this.btn技能1.Text = "普通攻击";
            this.btn技能1.UseVisualStyleBackColor = true;
            this.btn技能1.Click += new System.EventHandler(this.btn技能1_Click);
            // 
            // btn技能2
            // 
            this.btn技能2.Location = new System.Drawing.Point(294, 160);
            this.btn技能2.Name = "btn技能2";
            this.btn技能2.Size = new System.Drawing.Size(98, 43);
            this.btn技能2.TabIndex = 6;
            this.btn技能2.Text = "封魔斩";
            this.btn技能2.UseVisualStyleBackColor = true;
            this.btn技能2.Click += new System.EventHandler(this.btn技能2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(534, 160);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(98, 43);
            this.button3.TabIndex = 8;
            this.button3.Text = "技能4";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(414, 160);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(98, 43);
            this.button4.TabIndex = 7;
            this.button4.Text = "技能3";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // pg敌方魔法值
            // 
            this.pg敌方魔法值.BackColor = System.Drawing.SystemColors.Control;
            this.pg敌方魔法值.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pg敌方魔法值.Location = new System.Drawing.Point(498, 131);
            this.pg敌方魔法值.Name = "pg敌方魔法值";
            this.pg敌方魔法值.Size = new System.Drawing.Size(290, 11);
            this.pg敌方魔法值.TabIndex = 10;
            // 
            // pg我方魔法值
            // 
            this.pg我方魔法值.Location = new System.Drawing.Point(12, 131);
            this.pg我方魔法值.Name = "pg我方魔法值";
            this.pg我方魔法值.Size = new System.Drawing.Size(290, 11);
            this.pg我方魔法值.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pg敌方魔法值);
            this.Controls.Add(this.pg我方魔法值);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btn技能2);
            this.Controls.Add(this.btn技能1);
            this.Controls.Add(this.rich日志);
            this.Controls.Add(this.lbl敌方);
            this.Controls.Add(this.lbl我方);
            this.Controls.Add(this.pg敌方血量);
            this.Controls.Add(this.pg我方血量);
            this.Name = "MainForm";
            this.Text = "武侠世界-战斗窗口";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private 血量进度条 pg我方血量;
        private 血量进度条 pg敌方血量;
        private System.Windows.Forms.Label lbl我方;
        private System.Windows.Forms.Label lbl敌方;
        private System.Windows.Forms.RichTextBox rich日志;
        private System.Windows.Forms.Button btn技能1;
        private System.Windows.Forms.Button btn技能2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private 魔法进度条 pg敌方魔法值;
        private 魔法进度条 pg我方魔法值;
    }
}

