﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsGames
{
    public class 人
    {
        public string 姓名 { get; set; }

        public string 性别 { get; set; }

        public int 剩余血量 { get; set; } = 100;

        /// <summary>
        /// 魔法值用于施展技能
        /// </summary>
        public int 剩余魔法值 { get; set; } = 100;

        public void 普通攻击(人 目标者)
        {
            目标者.剩余血量 = 目标者.剩余血量 - 1;
            Console.WriteLine(this.姓名 + "攻击了" + 目标者.姓名 + "," + 目标者.姓名 + "剩余血量:" + 目标者.剩余血量);
        }

        public void 技能1(人 目标者)
        {
            int 使用的魔法值点数 = 10;
            if (this.剩余魔法值 < 使用的魔法值点数)//如果不够魔法值了
            {
                Console.WriteLine(this.姓名 + "使出了技能1，但是魔法值不够，哑火了");
                return;
            }
            this.剩余魔法值 = this.剩余魔法值 - 使用的魔法值点数;
            目标者.剩余血量 = 目标者.剩余血量 - 30;
            Console.WriteLine(this.姓名 + "攻击了" + 目标者.姓名+","+目标者.姓名+"剩余血量:"+目标者.剩余血量);
        }

        public void 技能2(人 目标者)
        {
            int 使用的魔法值点数 = 10;
            if (this.剩余魔法值 < 使用的魔法值点数)//如果不够魔法值了
            {
                Console.WriteLine(this.姓名 + "使出了技能2，但是魔法值不够，哑火了");
                return;
            }
            this.剩余魔法值 = this.剩余魔法值 - 使用的魔法值点数;
            目标者.剩余血量 = 目标者.剩余血量 - 20;
            Console.WriteLine(this.姓名 + "攻击了" + 目标者.姓名 + "," + 目标者.姓名 + "剩余血量:" + 目标者.剩余血量);
        }

        public void 技能3(人 目标者)
        {
            int 使用的魔法值点数 = 10;
            if (this.剩余魔法值 < 使用的魔法值点数)//如果不够魔法值了
            {
                Console.WriteLine(this.姓名 + "使出了技能2，但是魔法值不够，哑火了");
                return;
            }
            this.剩余魔法值 = this.剩余魔法值 - 使用的魔法值点数;
            目标者.剩余血量 = 目标者.剩余血量 - 50;
            Console.WriteLine(this.姓名 + "攻击了" + 目标者.姓名 + "," + 目标者.姓名 + "剩余血量:" + 目标者.剩余血量);
        }
    }
}
