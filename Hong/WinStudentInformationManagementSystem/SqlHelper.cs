﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinStudentInformationManagementSystem
{
    public class SqlHelper
    {
        public string StrSqlConn { get; set; }
        public SqlHelper()
        {
            StrSqlConn = "server=.;uid=sa;pwd=sa;Database=SIMS";
        }

        /// <summary>
        /// 增删改
        /// </summary>
        /// <param name="sql_command"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql_command)
        {
            var result = 0;
            using (SqlCommand cmd = new SqlCommand(sql_command, new SqlConnection(StrSqlConn)))
            {
                cmd.Connection.Open();
                result = cmd.ExecuteNonQuery();
                cmd.Connection.Close();
            }
            return result;
        }

        /// <summary>
        /// 查询表格
        /// </summary>
        /// <param name="sql_command"></param>
        /// <returns></returns>
        public DataTable ExecuteQuery(string sql_command)
        {
            var dt = new DataTable();
            using(SqlDataAdapter adapter = new SqlDataAdapter(sql_command, StrSqlConn))
            {
                adapter.Fill(dt);
            }
            return dt;
        }

    }
}
