﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinStudentInformationManagementSystem
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入用户名");
                return;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入密码");
                return;
            }

            var dt = new SqlHelper().ExecuteQuery($@"select a.*,b.RoleName from Users a
left join Roles b on a.RoleID = b.ID where UserName='{txtUserName.Text}' " +
                $"and Password='{txtPassword.Text}'");

            if (dt.Rows.Count > 0)
            {
                var role = dt.Rows[0]["RoleName"].ToString();
                this.Hide();

                switch (role)
                {
                    case "管理员":
                        new FrmAdminMainForm().ShowDialog();
                        break;
                    case "教师":
                        new FrmTeacherMainForm().ShowDialog();
                        break;
                    case "学生":
                        new FrmStudentMainForm().ShowDialog();
                        break;
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("登录失败");
            }

        }

        private void linkRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new FrmRigster().ShowDialog();
        }
    }
}
