﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinStudentInformationManagementSystem
{
    public partial class FrmRigster : Form
    {
        public FrmRigster()
        {
            InitializeComponent();
        }

        private void FrmRigster_Load(object sender, EventArgs e)
        {
            var dt = new SqlHelper()
                .ExecuteQuery("select * from Roles where RoleName!='管理员'");
            cmbRole.DataSource = dt;
            cmbRole.DisplayMember = "RoleName";
            cmbRole.ValueMember= "ID";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入用户名");
                return;
            }
            if (txtPassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("请输入密码");
                return;
            }
            if (txtPassword.Text != txtSecondPassword.Text)
            {
                MessageBox.Show("密码不一致,请确认");
                return;
            }

            new SqlHelper()
                .ExecuteNonQuery($"insert into Users (UserName,Password,RoleID) values ('{txtUserName.Text}','{txtPassword.Text}','{cmbRole.SelectedValue}')");
            MessageBox.Show("注册成功!");
            this.Close();

        }
    }
}
