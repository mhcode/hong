﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinStudentInformationManagementSystem
{
    public partial class FrmAdminMainForm : Form
    {
        public FrmAdminMainForm()
        {
            InitializeComponent();
        }

        private void FrmAdminMainForm_Load(object sender, EventArgs e)
        {
            var dt = new SqlHelper().ExecuteQuery(@"select a.ID,a.UserName,b.RoleName from Users a
left join Roles b on a.RoleID=b.ID");
            gridUsers.DataSource = dt;
        }
    }
}
