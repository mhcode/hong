﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ConsoleAppDataBase
{
    class Program
    {
        static void Main(string[] args)
        {
            var cmd = new SqlCommand("insert into 学生信息表 (姓名) values ('赵六')", new SqlConnection("server=.;uid=sa;pwd=sa;database=学生管理系统数据库"));
            cmd.Connection.Open();
            //var result = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            //Console.WriteLine("受影响的行数:" + result);//新增、修改、删除都可以用的方法

            DataTable dt = new DataTable();//单个表格
            DataSet ds = new DataSet();//多个表格,多个DataTable组成的对象

            SqlDataAdapter adapter = new SqlDataAdapter("select * from 学生信息表", new SqlConnection("server=.;uid=sa;pwd=sa;database=学生管理系统数据库"));
            adapter.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine("学生信息表有:" + row["姓名"].ToString());
            }

            Console.ReadLine();
        }
    }
}
