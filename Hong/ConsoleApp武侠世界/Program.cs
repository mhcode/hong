﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConsoleApp武侠世界
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入1使用普通攻击,输入2使用技能1");

            var hongzhi = new 人();
            hongzhi.姓名 = "宏智";
            hongzhi.性别 = "男";

            var 坏人 = new 人();
            坏人.姓名 = "XXX";

            while (true)
            {//true为继续循环

                var input = Console.ReadLine();
                if (input == "1")
                {
                    hongzhi.普通攻击(坏人);
                }
                if (input == "2")
                {
                    hongzhi.技能1(坏人);
                }

                Random random = new Random();
                var result = random.Next(3);
                if (result == 0)
                {
                    坏人.普通攻击(hongzhi);
                }
                if (result == 1)
                {
                    坏人.技能1(hongzhi);
                }
                if (result == 2)
                {
                    坏人.技能2(hongzhi);
                }
                if (result == 3)
                {
                    坏人.技能3(hongzhi);
                }

            }

            Console.ReadLine();
        }
    }
}
